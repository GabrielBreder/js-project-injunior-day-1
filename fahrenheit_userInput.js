var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Qual a temperatura em Fahrenheit? ", fahrenheit => {
  calcularTemperaturaCelcius(fahrenheit)
  
  rl.close();
});

function calcularTemperaturaCelcius(fahrenheit) {
  let celcius = (fahrenheit - 32) * 5 / 9

  console.log(`A temperatura em celcius é ${celcius.toFixed(3)}`)
}
