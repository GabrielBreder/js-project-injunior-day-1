for (let i = 0; i < 5; i++) {
  let numero = Math.round(Math.random()*100);

  if (numero % 3 === 0 && numero % 5 === 0) {
    console.log('fizzbuzz');
  } else if (numero % 3 === 0) {
    console.log('fizz');
  } else if (numero % 5 === 0) {
    console.log('buzz');
  } else {
    console.log(numero)
  }
}

